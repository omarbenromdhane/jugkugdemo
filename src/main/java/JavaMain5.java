// strategy patters
//public class JavaMain5 {
//    public static void main(String[] args) {
//        List<Integer> numbers=List.of(1,2,3,4,5,6,7);
//
////        total all the numbers
//        System.out.println(
//                totalValues(numbers)
//        );
//
////        total even values also
//        System.out.println(
//                totalEvenValues(numbers)
//        );
//
////        total odd values
//        System.out.println(
//                totalOddValues(numbers)
//        );
//    }
//
//    public static int totalValues(List<Integer> numbers){
//        int total=0;
//        for (Integer number : numbers) {
//            total+=number;
//        }
//        return total;
//    }
//
//    public static int totalEvenValues(List<Integer> numbers){
//        int total=0;
//        for (Integer number : numbers) {
//            if(number%2==0)
//                total+=number;
//        }
//        return total;
//    }
//
//    public static int totalOddValues(List<Integer> numbers){
//        int total=0;
//        for (Integer number : numbers) {
//            if(number%2!=0)
//                total+=number;
//        }
//        return total;
//    }
//
//}

import java.util.List;
import java.util.function.Predicate;

public class JavaMain5 {
    public static void main(String[] args) {

        List<Integer> numbers=List.of(1,2,3,4,5,6,7);

//        total all element
        System.out.println(
        totalValues(numbers,number->true)
        );

//        total even elements
        System.out.println(
                totalValues(numbers,number->number%2==0)
        );

//        total odd elements
        System.out.println(
                totalValues(numbers,number->number%2!=0)
        );
    }

//    public static int totalValues(List<Integer> numbers,
//                                  Predicate<Integer> selector){
//        int total=0;
//        for (Integer number : numbers) {
//            if(selector.test(number))
//                total+=number;
//        }
//        return total;
//    }

    public static int totalValues(List<Integer> numbers,
                                  Predicate<Integer> selector){
        return
                numbers.stream()
                        .filter(selector)
//                        .reduce(0,(c,e)->c+e);
                        .reduce(0, Integer::sum);
    }
}