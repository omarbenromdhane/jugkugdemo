

public class JavaMain10 {
    public static void main(String[] args) {

//        java knows the generic information for add method

//        List<Integer> numbers=new ArrayList<Integer>();
//        numbers.add(1);
//        numbers.add(2);
////        numbers.<Integer>add(3);
////        numbers.add("hello");
//        System.out.println(numbers);

//        java introduced the diamond operator

//        List<Integer> numbers=new ArrayList<>();
//        numbers.add(1);
//        numbers.add(2);
//        numbers.add(3);
////        numbers.add("hello");
//        System.out.println(numbers);


//        lambda type inference

//        List<Integer> numbers= Arrays.asList(1,2,3);

//        numbers.forEach((Integer e)-> System.out.println(e));
//        numbers.forEach((e)-> System.out.println(e));
//        numbers.forEach(e-> System.out.println(e));
//        numbers.forEach(e-> System.out.println(e.foo()));

//        limitation: annotation will not work with type inference (but when will be use annotations on lambdas ?)

//        numbers.forEach((final Integer e)-> System.out.println(e));
//        numbers.forEach((@NotNull Integer e)-> System.out.println(e));
//        numbers.forEach((final e)-> System.out.println(e));
//        numbers.forEach((@NotNull e)-> System.out.println(e));

//        giving the compiler enough context to infer the type

//        List<Person> persons =
//                List.of(
//                        new Person("sara", 20),
//                        new Person("sara", 22),
//                        new Person("bob", 20),
//                        new Person("paula", 32),
//                        new Person("paul", 32),
//                        new Person("jack", 3),
//                        new Person("jack", 72),
//                        new Person("jill", 11)
//                );

//        sort people by name

//        persons.stream()
////                .sorted(comparing((Person p) -> p.getName() ))
//                .sorted(comparing(p -> p.getName() ))//stream<Person>.sorted(Comparator<Person>)
//                .forEach(System.out::println);

//        sort people by name in reverse order

//        persons.stream()
//                .sorted(
//                        comparing((Person p)->p.getName())
//                        .reversed()
//                )
//                .forEach(System.out::println);

//        persons.stream()
//                .sorted(
//                        comparing(p->p.getName())
//                                .reversed()
//                )
//                .forEach(System.out::println); //assume it is an object because no enough context

//        persons.stream()
//                .sorted(
//                        comparing(Person::getName)
//                                .reversed()
//                )
//                .forEach(System.out::println); //the compiler get a type hint from the method reference

//        local variable type inference

//        String greet="hello";
//        var greet="hello";
//        var greet=null;

//        System.out.println(greet);

//        var greet="hello";
//        System.out.println(greet.getClass());

//        String greet;
//        var greet;
//        greet="hi";
//        System.out.println(greet);

//        context and type must be 100% clear for both programmer and compiler to be able to use type inference

//        what i gain if i use type inference
//        HashMap<String, Map<Integer, List<Double>>> variable = new HashMap<String, Map<Integer, List<Double>>>();
//        var variable = new HashMap<String, Map<Integer, List<Double>>>();

//        var names=List.of(
//                "Dory",
//                "Gill",
//                "Bruce",
//                "Nemo",
//                "Darla",
//                "Marlin",
//                "Jacques"
//        );

//        for (int i = 0; i < names.size(); i++) {
//            System.out.println(names.get(i));
//        }

//        for (var i = 0; i < names.size(); i++) {
//            System.out.println(names.get(i));
//        }

//        for (String name : names) {
//            System.out.println(name);
//        }

//        for (var name : names) {
//            System.out.println(name);
//        }

//        try(FileReader reader=new FileReader( "a file ")){
////            do sth
//        } catch (FileNotFoundException e) {
//            e.printStackTrace();
//        } catch (IOException e) {
//            e.printStackTrace();
//        }

//        try(var reader=new FileReader( "a file ")){
////            do sth
//        } catch (FileNotFoundException e) {
//            e.printStackTrace();
//        } catch (IOException e) {
//            e.printStackTrace();
//        }

//        type inference is not YET used with a class field // class x{ private var field1="hello";}

//        don't use var with diamond
//        List<Integer> numbers=new ArrayList<>();
//        var numbers=new ArrayList<>();
//        numbers.add(1);
//        numbers.add(2);
//        numbers.add("hello");
//        System.out.println(numbers);

//        mistake from java 5 will be turned to unexpected behavior when using var
//        do u want the type to be a class type or interface type ?

//        List<Integer> numbers=new ArrayList<Integer>(Arrays.asList(1,2,3));
//        System.out.println(numbers);
//        numbers.remove(1);
//        System.out.println(numbers);

//        Collection<Integer> numbers=new ArrayList<Integer>(Arrays.asList(1,2,3));
//        System.out.println(numbers);
//        numbers.remove(1);
//        System.out.println(numbers);

//        var numbers=new ArrayList<Integer>(Arrays.asList(1,2,3));
//        System.out.println(numbers);
//        numbers.remove(1);
//        System.out.println(numbers);

//        you can't store lambda using var

//        Function<Integer,Integer> lambda=e->e*2;
//        var lambda=e->e*2;
//        System.out.println(lambda.apply(5));

//        Runnable runnable=()-> System.out.println("hello");
//        var runnable=()-> System.out.println("hello");
//        var runnable=(Runnable)()-> System.out.println("hello");//DON'T DO THIS-FEEL STUPID
//        runnable.run();

//        easy to reuse

//        List<Person> persons =
//                List.of(
//                        new Person("sara", 20),
//                        new Person("sara", 22),
//                        new Person("bob", 20),
//                        new Person("paula", 32),
//                        new Person("paul", 32),
//                        new Person("jack", 3),
//                        new Person("jack", 72),
//                        new Person("jill", 11)
//                );

//        System.out.println(
//                persons.stream()
//                .collect(
//                        groupingBy(
//                                Person::getName,
//                                mapping(
//                                        Person::getAge,
//                                        toList()
//                                )
//                        )
//                )
//        );

//        var groupingByNameAndMapToAge=
//                groupingBy(
//                        Person::getName,
//                        mapping(
//                                Person::getAge,
//                                toList()
//                        )
//                );//do i care about the type of this function?!
//
//        System.out.println(
//                persons.stream()
//                        .collect(
//                                groupingByNameAndMapToAge
//                        )
//        );

//        targeted intersection type

////        List<Object> values=List.of(1,"hello");
//        var values=List.of(1,"hello");
//        try {
////            values.add(2);
////            values.add(new JavaMain10());
//        }
//        catch (Exception e){
//            e.printStackTrace();
//        }



    }
}
