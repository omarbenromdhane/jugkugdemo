import java.util.List;

public class JavaMain8 {
    public static void main(String[] args) {
        List<Person> persons =
                List.of(
                        new Person("sara", 20),
                        new Person("sara", 22),
                        new Person("bob", 20),
                        new Person("paula", 32),
                        new Person("paul", 32),
                        new Person("jack", 3),
                        new Person("jack", 72),
                        new Person("jill", 11)
                );

        use(persons);
    }

    public static void use(final List<Person> persons) {

//        total ages

//        System.out.println(
//                persons.stream()
//                        .mapToInt(Person::getAge)
////                        .reduce(0, (total, age) -> total + age)
////                        .reduce(0, (total, age) -> Integer.sum(total , age))
////                        .reduce(0, Integer::sum)
////                .sum()
//        );

//        first 3 persons

//        persons.stream()
//                .limit(3)
//                .forEach(System.out::println);

//        first 3 persons after skipping 2

//        persons.stream()
//                .skip(2)
//                .limit(3)
//                .forEach(System.out::println);

//        people until i find a paul

//        persons.stream()
//                .takeWhile(person -> person.getName()!="paul")
//                .forEach(System.out::println);

//        people after paul

//        persons.stream()
//                .dropWhile(person -> person.getName()!="paul")
//                .forEach(System.out::println);

//        list of names in uppercase of those who are older than 30

//        List<String> nameOfOlderThan30= new ArrayList<>();
//
//        persons.stream()
//                .filter(person -> person.getAge()>30)
//                .map(Person::getName)
//                .map(String::toUpperCase)
//                .forEach(name->nameOfOlderThan30.add(name));//KARTHA
//
//        System.out.println(nameOfOlderThan30);

//        System.out.println(
//                persons.stream()
//                        .filter(person -> person.getAge() > 30)
//                        .map(Person::getName)
//                        .map(String::toUpperCase)
//                        .reduce(
//                                new ArrayList<String>(),//initial value
//                                (names, name) -> {
//                                    names.add(name);
//                                    return names;
//                                },
//                                (names1, names2) -> {//parallel combine
//                                    names1.addAll(names2);
//                                    return names1;
//                                }
//                        )
//        );

//        System.out.println(
//                persons.stream()
//                        .filter(person -> person.getAge() > 30)
//                        .map(Person::getName)
//                        .map(String::toUpperCase)
////                        .collect(Collectors.toList())
//                        .collect(Collectors.toUnmodifiableList())
//        );

//        all distinct ages

//        System.out.println(
//                persons.stream()
//                        .map(Person::getAge)
////                        .distinct()
////                        .collect(Collectors.toUnmodifiableList())
//                        .collect(Collectors.toUnmodifiableSet())
//        );

//        given the names , print all names of length 4 , in uppercase , comma separated

//        String result="";
//        for (Person person : persons) {
//            if(person.getName().length()==4)
//                result+= person.getName().toUpperCase()+", ";
//        }
//        System.out.println(result);

//        System.out.println(
//                persons.stream()
//                        .map(Person::getName)
//                        .filter(name -> name.length() == 4)
//                        .map(String::toUpperCase)
//                        .collect(Collectors.joining(", "))
//        );

        //        even numbered age people and odd numbered age people

//        System.out.println(
//                persons.stream()
//                        .filter(person -> person.getAge() % 2 == 0)
//                        .collect(toUnmodifiableList())
//        );
//        System.out.println(
//                persons.stream()
//                        .filter(person -> person.getAge() % 2 != 0)
//                        .collect(toUnmodifiableList())
//        );

//        System.out.println(
//                persons.stream()
//                .collect(Collectors.partitioningBy(person -> person.getAge()%2==0))
//        );

//        group people based on their name

//        Map<String, List<Person>> byName = new HashMap<>();
//
//        for (Person person : persons) {
//            List<Person> list = null;
//
//            if (byName.containsKey(person.getName()))
//                list = byName.get(person.getName());
//            else {
//                list = new ArrayList<>();
//                byName.put(person.getName(), list);
//            }
//            list.add(person);
//        }
//
//        System.out.println(byName);

//        System.out.println(
//                persons.stream()
//                        .collect(Collectors.groupingBy(Person::getName))
//        );


//        group ages basing on names

//        System.out.println(
//                persons.stream()
//                        .collect(
//                                Collectors.groupingBy(
//                                        Person::getName,
//                                        Collectors.mapping(
//                                                Person::getAge,
//                                                Collectors.toUnmodifiableList()
//                                        )
//                                )
//                        )
//        );

//        count nbr occurrence for each name

//        System.out.println(
//                persons.stream()
//                .collect(
//                        Collectors.groupingBy(
//                                Person::getName,
//                                Collectors.counting()
//                        )
//                )
//        );

//        note , the previous operation return a long as occurrence nbr

//        groupingBy (function,collector)
//        collectingAndThen(collector, function)

//        System.out.println(
//                persons.stream()
//                        .collect(
//                                Collectors.groupingBy(
//                                        Person::getName,
//                                        Collectors.collectingAndThen(
//                                                Collectors.counting(),
//                                                Long::intValue
//                                        )
//                                )
//                        )
//        );

//        oldest person

//        System.out.println(
//                persons.stream()
//                .max(Comparator.comparingInt(Person::getAge))
//        );

//        order the list

//        persons.stream()
//                .sorted(Comparator.comparingInt(Person::getAge))
//                .forEach(System.out::println);
//
//        persons.stream()
//                .sorted(Comparator.comparingInt(Person::getAge).reversed())
//                .forEach(System.out::println);

//        order by age then by name

//        persons.stream()
//                .sorted(
//                        Comparator.comparingInt(Person::getAge)
//                                .thenComparing(Person::getName)
//                )
//                .forEach(System.out::println);

//        group names by age but show only names that starts with b

//        System.out.println(
//                persons.stream()
//                        .collect(
//                                Collectors.groupingBy(
//                                        Person::getAge,
//                                        Collectors.mapping(
//                                                Person::getName,
//                                                Collectors.filtering(
//                                                        name -> name.startsWith("b"),
//                                                        Collectors.toUnmodifiableList()
//                                                )
//                                        )
//                                )
//                        )
//        );

    }
}

final class Person {
    private final String name;
    private final int age;

    Person(String name, int age) {
        this.name = name;
        this.age = age;
    }

    @Override
    public String toString() {
        return "Person{" +
                "name='" + name + '\'' +
                ", age=" + age +
                '}';
    }

    public String getName() {
        return name;
    }

    public int getAge() {
        return age;
    }
}
