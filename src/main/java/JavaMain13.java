import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.Random;

public class JavaMain13 {
    public static void main(String[] args) {
//        IntStream.range(0,10)
//                .mapToObj(index->new Tuple<>(index,index*100))
//                .forEach(System.out::println);

//        var tup1=new Tuple<>("hi", 5);
//        System.out.println(
//                tup1.getElement1()
//        );

//        Stream.of(1,2,3,4,5)
//                .map(API.unchecked(index->doubleIt(index)))
//                .forEach(System.out::println);

//        var divider=5;
//        API.Try(()->(5/divider))
//                .onSuccess(System.out::println)
//                .onFailure(it-> System.out.println("oops "+it.getMessage()));

//        var divider=0;
//        API.Try(()->(5/divider))
//                .onSuccess(System.out::println)
//                .onFailure(it-> System.out.println("oops "+it.getMessage()));

//        var elem=5;
//        System.out.println(elem);
//        elem=6;
//        System.out.println(elem);

//        final var elem=5;
//        System.out.println(elem);
//        elem=6;
//        System.out.println(elem);

//        val elem=5;
//        System.out.println(elem);
//        elem=6;
//        System.out.println(elem);

//        val elem=5;
//        System.out.println(elem);
    }

    public static int doubleIt(final int number) throws RandomException {
        if (new Random().nextBoolean())
            throw new RandomException("this is a random exception");
        return number * 2;
    }
}

@Data
@AllArgsConstructor
final class Tuple<T1,T2>{
    private final T1 element1;
    private final T2 element2;
}
