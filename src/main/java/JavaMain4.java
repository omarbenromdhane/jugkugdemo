//interface Fly{
//    public void takeOff();
//    public void turn();
//    public void cruse();
//    public void land();
//}

// implement a method in the interface

interface Fly{
    default public void takeOff(){
        System.out.println("Fly::takeOff");
    }
    default public void turn(){
        System.out.println("Fly::turn");
    }
    default public void cruse(){
        System.out.println("Fly::cruse");
    }
    default public void land(){
        System.out.println("Fly::land");
    }
}
//rule number 1 : default methods are automatically inherited
//rule number 2 : u can override a default method

interface FastFly extends Fly{
    @Override
    default void takeOff() {
        System.out.println("FastFly::takeOff");
    }
}

//rule number 3 : methods in a class hierarchy rules

class Vehicle {
    public void land(){
        System.out.println("Vehicle::land");
    }
}

//class SeaPlane extends Vehicle implements FastFly{
//
//}
//
//public class JavaMain4 {
//    public void use(){
//        SeaPlane seaPlane=new SeaPlane();
//        seaPlane.takeOff();
//        seaPlane.turn();
//        seaPlane.cruse();
//        seaPlane.land();
//    }
//    public static void main(String[] args) {
//        new JavaMain4().use();
//    }
//}

interface Sail {
    default public void cruse(){
        System.out.println("Sail::cruise");
    }
}
//class SeaPlane extends Vehicle implements FastFly,Sail{
//
//}

class SeaPlane extends Vehicle implements FastFly,Sail{
    public void cruse(){
        System.out.println("SeaPlane::cruse");
        FastFly.super.cruse();
//        Sail.super.cruse();
//        why to call super? because interfaces have static methods now
    }
}

public class JavaMain4 {
    public void use(){
        SeaPlane seaPlane=new SeaPlane();
        seaPlane.takeOff();
        seaPlane.turn();
        seaPlane.cruse();
        seaPlane.land();
    }
    public static void main(String[] args) {
        new JavaMain4().use();
    }
}

// why we can't throw abstract classes?
//1-interfaces cannot have state --no fields in interfaces
//2-u block the inheritance hierarchy when using abstract class
// while u can implement many interfaces