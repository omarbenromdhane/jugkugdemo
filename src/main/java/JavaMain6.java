import java.util.Optional;
import java.util.Random;

public class JavaMain6 {
    public static void main(String[] args) {
//                randomOptional()
//                        .ifPresentOrElse(
//                                number-> System.out.println("we get a "+number),
//                                ()-> System.out.println("we get nothing")
//                        );

//        randomOptional()
//                .map(number->"the string form of the number is "+number)
//                .ifPresentOrElse(
//                        number-> System.out.println("we get a "+number),
//                        ()-> System.out.println("we get nothing")
//                );

//        System.out.println(
//                randomOptional()
//                .orElse(0)
//        );

//        System.out.println(
//                randomOptional()
//                        .orElse(defaultValue())
//        );

        System.out.println(
                randomOptional()
                        .orElseGet(()->defaultValue())
        );

//        get is to forget
    }

    public static Optional<Integer> randomOptional(){
        if(new Random().nextInt(2)>=1)
            return Optional.of(new Random().nextInt());
        return Optional.empty();
    }

    public static int defaultValue(){
        System.out.println("default value is called");
        return 11;
    }
}
