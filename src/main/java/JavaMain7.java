import java.time.Duration;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class JavaMain7 {
    public static void main(String[] args) {
        System.out.println(
                LocalDateTime.now().format(DateTimeFormatter.BASIC_ISO_DATE)
        );

        System.out.println(
                LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy/MM,dd HH:mm:ss"))
        );

        System.out.println(
                LocalDateTime.parse("2021/01,27 22:16:13",DateTimeFormatter.ofPattern("yyyy/MM,dd HH:mm:ss"))
        );

        System.out.println(
                Duration.between(LocalDateTime.now(), LocalDateTime.now().plusDays(5))
                .toDays()
        );
    }
}
