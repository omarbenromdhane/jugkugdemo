import java.util.List;

public class JavaMain9 {
    public static void main(String[] args) {

        List<List<String>> listOfListOfElements = List.of(
                List.of("a", "b", "c"),
                List.of("1", "2", "3"),
                List.of("#", "&", "@")
        );

//        concat ~ to all elements and merge them to single list

//        System.out.println(
//                listOfListOfElements.stream()
//                        .flatMap(list -> list.stream())
//                        .map(element -> element.concat("~"))
//                        .collect(Collectors.toUnmodifiableList())
//        );

//        create stream from elements

//        Stream.of("a", "b", "c")
//                .map(String::toUpperCase)
//                .forEach(System.out::println);

//        generate stream

//        Stream.iterate(0,i->i<10,i->i+1)
//                .map(i->i*11)
//                .forEach(System.out::println);

//        IntStream.range(0,10)
//                .map(i->i*11)
//                .forEach(System.out::println);

    }
}
