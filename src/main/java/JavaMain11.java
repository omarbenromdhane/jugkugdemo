public class JavaMain11 {
//    public static void main(String[] args) {
//        System.out.println(
//                JavaMain11.fullClassName("hello")
//        );
//        System.out.println(
//                JavaMain11.fullClassName(5)
//        );
//        System.out.println(
//                JavaMain11.fullClassName(new Object())
//        );
//
//        System.out.println(
//                JavaMain11.<String>fullClassName("hello")
//        );
//
//        System.out.println(
//                JavaMain11.<Integer>fullClassName("hello")
//        );
//    }
//
//    public static <T> String fullClassName(final T input){
//        return input.getClass().getTypeName();
//    }

    public static void main(String[] args) {

//        Pair<String,Integer> pairOfStringAndInteger=new Pair<>("hello",1);
//        Pair<String,String> pairOfStringAndString=new Pair("hello again", "hello another time");
//
//        System.out.println(pairOfStringAndInteger);
//        System.out.println(pairOfStringAndString);

//        IntStream.range(150,200)
//                .mapToObj(index->new Pair<>(index,index%2==0))
//                .forEach(System.out::println);

//        Pair<String,String> pairOfStringAndString =new Pair<>("hi","hello");
//        System.out.println(
////                pairOfStringAndString.doSth(5,"hi hello")
////                pairOfStringAndString.doSth("hi hi","hi hi hi ")
////                pairOfStringAndString.doSth("5",10)
////                pairOfStringAndString.<Integer>doSth("5","hola")
//        );

    }
}

final class Pair<T1,T2>{
    private final T1 element1;
    private final T2 element2;

    public T1 getElement1() {
        return element1;
    }

    @Override
    public String toString() {
        return "Pair{" +
                "element1=" + element1 +
                ", element2=" + element2 +
                '}';
    }

    public T2 getElement2() {
        return element2;
    }

    public Pair(T1 element1, T2 element2) {
        this.element1 = element1;
        this.element2 = element2;
    }

    public <I> boolean doSth(final T1 input1, final I input2){
        return input1.getClass()==input2.getClass();
    }
}
