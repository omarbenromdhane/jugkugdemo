import java.util.Random;
import java.util.function.Function;
import java.util.function.Supplier;

public class JavaMain12 {
    public static void main(String[] args)
//            throws RandomException {

//        System.out.println(
//                doubleIt(100)
//        );
    {

//        try {
//            System.out.println(
//                    doubleIt(100)
//            );
//        } catch (RandomException e) {
//            e.printStackTrace();
//        }

//        Stream.of(5,6,7,8,9)
//                .map(index->doubleIt(index))
//                .forEach(System.out::println);

//        Stream.of(5,6,7,8,9)
//                .map(index-> {
//                    try {
//                        return doubleIt(index);
//                    } catch (RandomException e) {
//                        e.printStackTrace();
//                    }
//                }
//                )
//                .forEach(System.out::println);


//        Stream.of(5,6,7,8,9)
//                .map(ThrowingFunction.unchecked(index -> doubleIt(index)))
//                .forEach(System.out::println);

//        Stream.of(5,6,7,8,9)
//                .map(index->doubleIt(index))
//                .filter(Either::isRight)
//                .map(Either::rightElement)
//                .forEach(System.out::println);

//        Stream.of(5,6,7,8,9)
//                .map(index->doubleIt(index))
//                .filter(Either::isLeft)
//                .map(Either::leftElement)
//                .forEach(System.out::println);

//        Stream.of(5,6,7,8,9)
//                .map(index -> Try.tryIt(()->(5/index)))
//                .filter(Try::isFailure)
//                .map(Try::getException)
//                .forEach(System.out::println);

//        Stream.of(5,6,7,8,9)
//                .map(index -> Try.tryIt(()->(10/index)))
//                .filter(Try::isSuccess)
//                .map(Try::getResult)
//                .forEach(System.out::println);

    }

    public static int doubleIt(final int number) throws RandomException {
        if (new Random().nextBoolean())
            throw new RandomException("this is a random exception");
        return number * 2;
    }

//    public static Either<Throwable,Integer> doubleIt(final int number) {
//        if (new Random().nextBoolean())
//            return Either.left( new RandomException("this is a random exception"));
//        return Either.right(number * 2);
//    }

}

final class RandomException extends Exception {
    public RandomException() {
        super();
    }

    public RandomException(final String message) {
        super(message);
    }

    public RandomException(final String message, final Throwable cause) {
        super(message, cause);
    }
}


@FunctionalInterface
interface ThrowingFunction<T, R, E extends Throwable> {
    R apply(T t) throws E;

    static <T, R, E extends Throwable> Function<T, R> unchecked(ThrowingFunction<T, R, E> f) {
        return t -> {
            try {
                return f.apply(t);
            } catch (Throwable e) {
                throw new RuntimeException(e);
            }
        };
    }
}


final class Either<T1, T2> {
    private final T1 leftElement;
    private final boolean left;
    private final T2 rightElement;
    private final boolean right;

    private Either() {
        leftElement = null;
        left = false;
        rightElement = null;
        right = false;
    }

    private Either(final T1 leftElement, final boolean left, final T2 rightElement, final boolean right) {
        this.leftElement = leftElement;
        this.left = left;
        this.rightElement = rightElement;
        this.right = right;
    }

    public static <I> Either left(final I leftElement) {
        return
                new Either<>(
                        leftElement,
                        true,
                        null,
                        false
                );
    }

    public static <I> Either right(final I rightElement) {
        return
                new Either<>(
                        null,
                        false,
                        rightElement,
                        true
                );
    }

    public T1 leftElement() {
        return leftElement;
    }

    public boolean isLeft() {
        return left;
    }

    public T2 rightElement() {
        return rightElement;
    }

    public boolean isRight() {
        return right;
    }
}

final class Try<T> {

    private final boolean success;
    private final boolean failure;
    private final T result;
    private final Exception exception;

    private Try() {
        success = false;
        failure = false;
        result = null;
        exception = null;
    }

    private Try(final boolean success, final T result, final Exception exception) {
        this.success = success;
        this.failure = !success;
        this.result = result;
        this.exception = exception;
    }

    public static <T> Try<T> tryIt(final Supplier<T> supplier) {
        try {
            return new Try<T>(true, supplier.get(), null);
        } catch (Exception exception) {
            return new Try<T>(false, null, exception);
        }
    }

    public boolean isSuccess() {
        return success;
    }

    public boolean isFailure() {
        return failure;
    }

    public T getResult() {
        return result;
    }

    public Exception getException() {
        return exception;
    }
}