

public class JavaMain1 {
    public static void main(String[] args) {
//        what are lambda expression
//        lambda expression are anonymous methods , cute methods
//        List<Integer> numbers= List.of(1,2,3,4,5,6);

//        complex , familiar and not simple
//        for (int i = 0; i < numbers.size(); i++) {
//            System.out.println(numbers.get(i));
//        }

//        for (Integer number : numbers) {
//            System.out.println(number);
//        }

//      external iterator vs  internal iterator
//        tell what to do and not how to do

//        anonymous inner class
//        numbers.forEach(new Consumer<Integer>() {
//            @Override
//            public void accept(Integer integer) {
//                System.out.println(integer);
//            }
//        });

//        compile and check

//        what is the most important part to keep from the function?

//        numbers.forEach(
//                (Integer integer) -> System.out.println(integer)
//        );

//        numbers.forEach(
//                (integer) -> System.out.println(integer)
//        );

//        numbers.forEach(integer -> System.out.println(integer) );

//        numbers.forEach(System.out::println);
    }
}
